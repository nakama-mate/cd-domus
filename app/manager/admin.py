from django.contrib import admin

###########################################
# Concept of Django Admin Database: CRUD
#
# CREATE -- Make new
# RETRIEVE -- Get -- List / Search
# UPDATE -- Edit
# DELETE -- Delete
###########################################
## Register your models here.
# from posts.models import Post
## Since admin and models models are in the same hierarchy
## so it can be abbreviated to as ".models" shown below.
## Reference: https://docs.djangoproject.com/en/2.1/ref/contrib/admin/
##########################################################################
from .models import Post

class PostModelAdmin(admin.ModelAdmin):
  list_display = ["title", "updatestamp", "timestamp"]

  ## Show registed item(s) as a clickable link.
  list_display_links = ["title"]

  ## Add filter(s) related with the registerd item(s) on the right side of
  ## admin webpage.
  list_filter = ["updatestamp"]

  ## Add item(s) to be searchable.
  search_fields = ["title", "content"]

  class Meta:
    model = Post

admin.site.register(Post, PostModelAdmin)