
{% load static %}

<div class="world-catagory-area">
  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="title">Latest</li>

    <li class="nav-item">
      <a class="nav-link active" id="tab0" data-toggle="tab" href="#world-tab-0" role="tab" aria-controls="world-tab-0" aria-selected="true">
        全部
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" id="tab1" data-toggle="tab" href="#world-tab-1" role="tab" aria-controls="world-tab-1" aria-selected="false">
        タブ1
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" id="tab2" data-toggle="tab" href="#world-tab-2" role="tab" aria-controls="world-tab-2" aria-selected="false">
        タブ2
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" id="tab3" data-toggle="tab" href="#world-tab-3" role="tab" aria-controls="world-tab-3" aria-selected="false">
        タブ3
      </a>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">その他</a>
      <div class="dropdown-menu">
        <a class="nav-link" id="tab4" data-toggle="tab" href="#world-tab-4" role="tab" aria-controls="world-tab-4" aria-selected="false">
          タブ6
        </a>
        <a class="nav-link" id="tab5" data-toggle="tab" href="#world-tab-5" role="tab" aria-controls="world-tab-5" aria-selected="false">
          タブ7
        </a>
      </div>
    </li>
  </ul>

  <div class="tab-content" id="myTabContent">

    <!-- Add class 'show' and 'active' and it opens -->
    <div id="world-tab-0" role="tabpanel" aria-labelledby="tab0" class="tab-pane fade show active">

      <!-- Single Blog Post -->
      <div class="single-blog-post post-style-4 d-flex align-items-center">
        <!-- Post Thumbnail -->
        <div class="post-thumbnail">
          <img src="/static/img/blog-img/b1.jpg" alt="">
        </div>
        <!-- Post Content -->
        <div class="post-content">
          <a href="#" class="headline">
            <h5>記事タイトル</h5>
          </a>
          <p>
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
          </p>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>

      <!-- Single Blog Post -->
      <div class="single-blog-post post-style-4 d-flex align-items-center">
        <!-- Post Thumbnail -->
        <div class="post-thumbnail">
          <img src="/static/img/blog-img/b2.jpg" alt="">
        </div>
        <!-- Post Content -->
        <div class="post-content">
          <a href="#" class="headline">
            <h5>記事タイトル</h5>
          </a>
          <p>
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
          </p>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>

      <!-- Single Blog Post -->
      <div class="single-blog-post post-style-4 d-flex align-items-center">
        <!-- Post Thumbnail -->
        <div class="post-thumbnail">
          <img src="/static/img/blog-img/b3.jpg" alt="">
        </div>
        <!-- Post Content -->
        <div class="post-content">
          <a href="#" class="headline">
            <h5>記事タイトル</h5>
          </a>
          <p>
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
          </p>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>

      <!-- Single Blog Post -->
      <div class="single-blog-post post-style-4 d-flex align-items-center">
        <!-- Post Thumbnail -->
        <div class="post-thumbnail">
          <img src="/static/img/blog-img/b4.jpg" alt="">
        </div>
        <!-- Post Content -->
        <div class="post-content">
          <a href="#" class="headline">
            <h5>記事タイトル</h5>
          </a>
          <p>
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
          </p>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>
    </div>

    <div id="world-tab-1" role="tabpanel" aria-labelledby="tab1" class="tab-pane fade">

      <!-- Single Blog Post -->
      <div class="single-blog-post post-style-4 d-flex align-items-center">
        <!-- Post Thumbnail -->
        <div class="post-thumbnail">
          <img src="/static/img/blog-img/b5.jpg" alt="">
        </div>
        <!-- Post Content -->
        <div class="post-content">
          <a href="#" class="headline">
            <h5>記事タイトル</h5>
          </a>
          <p>
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
          </p>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>

    </div>

    <div id="world-tab-2" role="tabpanel" aria-labelledby="tab2" class="tab-pane fade">

      <!-- Single Blog Post -->
      <div class="single-blog-post post-style-4 d-flex align-items-center">
        <!-- Post Thumbnail -->
        <div class="post-thumbnail">
          <img src="/static/img/blog-img/b6.jpg" alt="">
        </div>
        <!-- Post Content -->
        <div class="post-content">
          <a href="#" class="headline">
            <h5>記事タイトル</h5>
          </a>
          <p>
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
          </p>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>

    </div>

    <div id="world-tab-3" role="tabpanel" aria-labelledby="tab3" class="tab-pane fade">

      <!-- Single Blog Post -->
      <div class="single-blog-post post-style-4 d-flex align-items-center">
        <!-- Post Thumbnail -->
        <div class="post-thumbnail">
          <img src="/static/img/blog-img/b7.jpg" alt="">
        </div>
        <!-- Post Content -->
        <div class="post-content">
          <a href="#" class="headline">
            <h5>記事タイトル</h5>
          </a>
          <p>
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
          </p>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>

    </div>

    <div id="world-tab-4" role="tabpanel" aria-labelledby="tab4" class="tab-pane fade">

      <!-- Single Blog Post -->
      <div class="single-blog-post post-style-4 d-flex align-items-center">
        <!-- Post Thumbnail -->
        <div class="post-thumbnail">
          <img src="/static/img/blog-img/b8.jpg" alt="">
        </div>
        <!-- Post Content -->
        <div class="post-content">
          <a href="#" class="headline">
            <h5>記事タイトル</h5>
          </a>
          <p>
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
          </p>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>

    </div>

    <div id="world-tab-5" role="tabpanel" aria-labelledby="tab5" class="tab-pane fade">

      <!-- Single Blog Post -->
      <div class="single-blog-post post-style-4 d-flex align-items-center">
        <!-- Post Thumbnail -->
        <div class="post-thumbnail">
          <img src="/static/img/blog-img/b9.jpg" alt="">
        </div>
        <!-- Post Content -->
        <div class="post-content">
          <a href="#" class="headline">
            <h5>記事タイトル</h5>
          </a>
          <p>
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
            記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
          </p>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>

    </div>

  </div>
</div>
