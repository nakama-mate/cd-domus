
{% load static %}

<div class="hero-area height-500">

  <!-- Hero Slides Area -->
  <div class="hero-slides owl-carousel">
    <!-- Single Slide -->
    <div class="single-hero-slide bg-img background-overlay"
      style="background-image: url(/static/img/blog-img/bg1.jpg); height:500px;">
    </div>
    <!-- Single Slide -->
    <div class="single-hero-slide bg-img background-overlay"
      style="background-image: url(/static/img/blog-img/bg2.jpg); height:500px;">
    </div>
    <!-- Single Slide -->
    <div class="single-hero-slide bg-img background-overlay"
      style="background-image: url(/static/img/blog-img/bg3.jpg); height:500px;">
    </div>
    <!-- Single Slide -->
    <div class="single-hero-slide bg-img background-overlay"
      style="background-image: url(/static/img/blog-img/bg4.jpg); height:500px;">
    </div>
  </div>

  <!-- Hero Post Slide -->
  <div class="hero-title-area">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center">
        <div class="col-12 col-md-8 col-lg-6">
          <div class="single-blog-title text-center">
            <h1>CD-domus</h1>
            <h3>Django trial Website</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
