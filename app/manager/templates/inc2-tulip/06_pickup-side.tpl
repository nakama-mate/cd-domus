
<div class="sidebar-widget-area">
  <h5 class="title">Links</h5>
  <div class="widget-content">

    <!-- Single Blog Post -->
    <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <img src="/static/img/blog-img/b29.jpg" alt="">
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5 class="mb-0">リンクタイトルリンクタイトルリンクタイトルリンクタイトルリンクタイトルリンクタイトル</h5>
        </a>
      </div>
    </div>

    <!-- Single Blog Post -->
    <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <img src="/static/img/blog-img/b35.jpg" alt="">
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5 class="mb-0">リンクタイトルリンクタイトルリンクタイトルリンクタイトルリンクタイトルリンクタイトル</h5>
        </a>
      </div>
    </div>

    <!-- Single Blog Post -->
    <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <img src="/static/img/blog-img/b36.jpg" alt="">
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5 class="mb-0">リンクタイトルリンクタイトルリンクタイトルリンクタイトルリンクタイトルリンクタイトル</h5>
        </a>
      </div>
    </div>

  </div>
</div>
