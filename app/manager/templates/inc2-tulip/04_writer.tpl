
<div class="sidebar-widget-area">
  <h5 class="title">domus members</h5>
  <div class="widget-content">
    <p style="text-align:center;">
      <img src="/static/img/icons/avatar-karl.png" alt="" style="width:100px;">
    </p>
    <div class="social-area d-flex justify-content-between mb-30">
      <a href="#"><i class="fa fa-gitlab"></i></a>
      <a href="#"><i class="fa fa-twitter"></i></a>
      <a href="#"><i class="fa fa-instagram"></i></a>
      <a href="#"><i class="fa fa-ban"></i></a>
      <a href="#"><i class="fa fa-ban"></i></a>
      <a href="#"><i class="fa fa-ban"></i></a>
    </div>
    <p style="text-align:center;">
      <img src="/static/img/icons/avatar-midori.png" alt="" style="width:100px;">
    </p>
    <div class="social-area d-flex justify-content-between">
      <a href="#"><i class="fa fa-gitlab"></i></a>
      <a href="#"><i class="fa fa-twitter"></i></a>
      <a href="#"><i class="fa fa-instagram"></i></a>
      <a href="#"><i class="fa fa-ban"></i></a>
      <a href="#"><i class="fa fa-ban"></i></a>
      <a href="#"><i class="fa fa-ban"></i></a>
    </div>
  </div>
</div>
