
<div class="row">

  <div class="col-12 col-md-6 col-lg-4">
    <!-- Single Blog Post -->
    <div class="single-blog-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <img src="/static/img/blog-img/b19.jpg" alt="">
        <!-- Catagory -->
        <div class="post-cta"><a href="#"></a></div>
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5>記事タイトル</h5>
        </a>
        <p>
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
        </p>
        <!-- Post Meta -->
        <div class="post-meta">
          <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
        </div>
      </div>
    </div>
  </div>

  <div class="col-12 col-md-6 col-lg-4">
    <!-- Single Blog Post -->
    <div class="single-blog-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <img src="/static/img/blog-img/b20.jpg" alt="">
        <!-- Catagory -->
        <div class="post-cta"><a href="#"></a></div>
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5>記事タイトル</h5>
        </a>
        <p>
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
        </p>
        <!-- Post Meta -->
        <div class="post-meta">
          <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
        </div>
      </div>
    </div>
  </div>

  <div class="col-12 col-md-6 col-lg-4">
    <!-- Single Blog Post -->
    <div class="single-blog-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <img src="/static/img/blog-img/b21.jpg" alt="">
        <!-- Catagory -->
        <div class="post-cta"><a href="#"></a></div>
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5>記事タイトル</h5>
        </a>
        <p>
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
        </p>
        <!-- Post Meta -->
        <div class="post-meta">
          <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
        </div>
      </div>
    </div>
  </div>

  <div class="col-12 col-md-6 col-lg-4">
    <!-- Single Blog Post -->
    <div class="single-blog-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <img src="/static/img/blog-img/b22.jpg" alt="">
        <!-- Catagory -->
        <div class="post-cta"><a href="#"></a></div>
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5>記事タイトル</h5>
        </a>
        <p>
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭
          記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭記事冒頭...
        </p>
        <!-- Post Meta -->
        <div class="post-meta">
          <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
        </div>
      </div>
    </div>
  </div>

</div>
