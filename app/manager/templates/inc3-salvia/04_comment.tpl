
<div class="row justify-content-center">
  <div class="col-12 col-lg-8">
    <!-- Comment Area Start -->
    <div class="comment_area clearfix mt-70">
      <ol>
        <!-- Single Comment Area -->
        <li class="single_comment_area">
          <!-- Comment Content -->
          <div class="comment-content">
            <!-- Comment Meta -->
            <div class="comment-meta d-flex align-items-center justify-content-between">
              <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
              <a href="#" class="comment-reply btn world-btn">Reply</a>
            </div>
            <p>内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容</p>
          </div>
          <ol class="children">
            <li class="single_comment_area list-style-hidden">
              <!-- Comment Content -->
              <div class="comment-content">
                <!-- Comment Meta -->
                <div class="comment-meta d-flex align-items-center justify-content-between">
                  <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
                  <a href="#" class="comment-reply btn world-btn">Reply</a>
                </div>
                <p>内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容</p>
              </div>
            </li>
          </ol>
        </li>
        
        <!-- Single Comment Area -->
        <li class="single_comment_area">
          <!-- Comment Content -->
          <div class="comment-content">
            <!-- Comment Meta -->
            <div class="comment-meta d-flex align-items-center justify-content-between">
              <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
              <a href="#" class="comment-reply btn world-btn">Reply</a>
            </div>
            <p>内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容</p>
          </div>
        </li>
        
      </ol>
    </div>
  </div>
  <div class="col-12 col-lg-8">
    <div class="post-a-comment-area mt-30">
      <h5>コメントする</h5>
      <!-- Contact Form -->
      <form action="#" method="post">
        <div class="row">
          <div class="col-12 col-md-6">
            <div class="group">
              <input type="text" name="name" id="name" required>
              <span class="highlight"></span>
              <span class="bar"></span>
              <label>名前</label>
            </div>
          </div>
          <div class="col-12 col-md-6">
          </div>
          <div class="col-12">
            <div class="group">
              <textarea name="message" id="message" required></textarea>
              <span class="highlight"></span>
              <span class="bar"></span>
              <label>Enter your comment</label>
            </div>
          </div>
          <div class="col-12">
            <button type="submit" class="btn world-btn">Post comment</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
