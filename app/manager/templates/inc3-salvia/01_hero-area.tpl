
<div class="hero-area height-400 bg-img background-overlay"
    style="background-image: url(/static/img/blog-img/bg2.jpg);">
  <div class="container h-100">
    <div class="row h-100 align-items-center justify-content-center">
      <div class="col-12 col-md-8 col-lg-6">
        <div class="single-blog-title text-center">
          <!-- Catagory -->
          <div class="post-cta">
            <a href="#">Environment</a>
            <a href="#">Programming</a>
          </div>
          <h3>
            Test Article
          </h3>
        </div>
      </div>
    </div>
  </div>
</div>
