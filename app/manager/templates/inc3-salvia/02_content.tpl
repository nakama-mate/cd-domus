
<!-- Post Meta -->
<div class="post-meta">
  <p><a href="#" class="post-author">Chan</a> on <a href="#" class="post-date">Sep 29, 2017</a></p>
</div>
<!-- Post Content -->
<div class="post-content">

  <div class="markdown-body">
    <!-- Article from here -->

    <h1>
    <a id="user-content-test-articleh1" class="anchor" href="#test-articleh1" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Test Article(H1)</h1>
    <h2>
    <a id="user-content-youh2" class="anchor" href="#youh2" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>You(H2)</h2>
    <h3>
    <a id="user-content-can-writeh3" class="anchor" href="#can-writeh3" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Can write(H3)</h3>
    <h4>
    <a id="user-content-articlesh4" class="anchor" href="#articlesh4" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Articles(H4)</h4>
    <h5>
    <a id="user-content-likeh5" class="anchor" href="#likeh5" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Like(H5)</h5>
    <h6>
    <a id="user-content-thish6" class="anchor" href="#thish6" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>this!(H6)</h6>
    <p>You can learn how to write markdown text here (<a href="https://gist.github.com/mignonstyle/083c9e1651d7734f84c99b8cf49d57fa">markdown-cheatsheet.md</a>).<br>
    <strong>OmniMarkupPreviewer</strong> Sublime Plugin supports you to write markdown.</p>
    <hr>
    <ol>
    <li>Write an article with markdown
    <ul>
    <li>Put the markdown file as <code>MarkdownToHtml/from.md</code>
    </li>
    </ul>
    </li>
    <li>Run <code>MarkdownToHtml.py</code>
    </li>
    <li>You'll get html-nized article as <code>to.html</code> file.</li>
    </ol>
    <hr>
    <p>To write table on markdown text, <strong>Table Editor</strong> Sublime Plugin helps you.</p>
    <table>
    <thead>
    <tr>
    <th align="center"></th>
    <th align="left">HTML</th>
    <th align="left">Markdown</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td align="center">Takes time</td>
    <td align="left">YES</td>
    <td align="left">NO</td>
    </tr>
    <tr>
    <td align="center">Cool</td>
    <td align="left">NO</td>
    <td align="left">YES</td>
    </tr>
    <tr>
    <td align="center">Readable</td>
    <td align="left">NO</td>
    <td align="left">YES</td>
    </tr>
    </tbody>
    </table>
    <blockquote>
    <p>Markdown is gentle men's Hobby.</p>
    <blockquote>
    <p>Great person</p>
    </blockquote>
    </blockquote>
    <p><a href="/static/img/logos/CD-domus-logo4.png" target="_blank" rel="noopener noreferrer"><img src="/static/img/logos/CD-domus-logo4.png" alt="CD-domus-logo" style="max-width:100%;"></a></p>
    <p><em><strong>I'm sure you wanna know how to write code in the article.</strong></em></p>
    <pre><code>Normal code.
    </code></pre>
    <div class="highlight highlight-source-python"><pre class="bg-beige"><span class="pl-c"><span class="pl-c">#</span> Python</span>
    <span class="pl-k">def</span> <span class="pl-en">foo</span>(<span class="pl-smi">x</span>):
        <span class="pl-k">return</span> x <span class="pl-k">==</span> <span class="pl-c1">0</span></pre></div>
    <div class="highlight highlight-source-cs"><pre class="bg-lightgreen"><span class="pl-c"><span class="pl-c">//</span> C#</span>
    <span class="pl-smi">function</span> <span class="pl-k">bool</span> <span class="pl-en">bar</span>(<span class="pl-k">int</span> <span class="pl-smi">x</span>)
    {
        <span class="pl-k">return</span> <span class="pl-smi">x</span> <span class="pl-k">==</span> <span class="pl-c1">0</span>
    }</pre></div>
    <hr>
    <ul>
    <li>Youtube video</li>
    </ul>
    <p>
      <a href="http://www.youtube.com/watch?v=aohzKJ9pNj4" rel="nofollow"><img src="https://camo.githubusercontent.com/a2955e37cbae7c7ab91a8fd2cc57e186db70e198/687474703a2f2f696d672e796f75747562652e636f6d2f76692f616f687a4b4a39704e6a342f302e6a7067" alt="IMAGE ALT TEXT HERE" data-canonical-src="http://img.youtube.com/vi/aohzKJ9pNj4/0.jpg" style="max-width:100%;"></a></p>
    <p>
      日本語の文章だよ。はたしてどんな感じに表示されるのか。
    </p>
    <p>End.</p>

    <!-- Article ends here. -->
  </div>

  <!-- Post Tags -->
  <ul class="post-tags">
    <li><a href="#">Tag A</a></li>
    <li><a href="#">Tag B</a></li>
    <li><a href="#">Tag C</a></li>
    <li><a href="#">Tag D</a></li>
  </ul>
  <!-- Post Meta -->
  <div class="post-meta second-part">
    <p><a href="#" class="post-author">Chan</a> on <a href="#" class="post-date">Sep 29, 2017</a></p>
  </div>

  <div class="widget-content mt-30">
    <div class="social-area d-flex justify-content-between">
      <a href="#"><i class="fa fa-facebook"></i></a>
      <a href="#"><i class="fa fa-twitter"></i></a>
      <a href="#"><i class="fa fa-pinterest"></i></a>
      <a href="#"><i class="fa fa-vimeo"></i></a>
      <a href="#"><i class="fa fa-instagram"></i></a>
      <a href="#"><i class="fa fa-google"></i></a>
    </div>
  </div>

</div>
