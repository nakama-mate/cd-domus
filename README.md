
CD-domus
===

Joint development of web app.

![1](media/CD-domus-logo4.png)

## Installation

![2](media/readme-image.jpg)

### Process to setup VM environment

1. Pull latest master branch.
2.  Download and install them two.
    - [Vagrant](https://www.vagrantup.com/)
    - [Virtualbox](https://www.virtualbox.org/)
3. Open the console on the top directory. Create VM. It would take some time to download CentOS file.
    - `$ vagrant up`
    - ![3](media/vagrant-up.jpg)
4. Login to your VM.
    - `$ vagrant ssh`
5. Launch django server. (In the VM.)
    - `$ python3.6 /vagrant/app/manage.py runserver 0.0.0.0:8000`
6. Open django page via browser on your local machine, not VM. I set the Vagrant leads your access through port 9927 to the port 8000 on the VM.
    - [http://localhost:9927/](http://localhost:9927/)

### Ways to handle VM

```bash
# Out from VM ssh console.
$ exit

# Shutdown the VM.
$ vagrant halt

# Check the VM status.
$ vagarnt status

# Open VM again.
$ vagrant up
```
