#!/bin/sh


echo '----- Install ius-release.rpm -----'
yum install -y https://centos7.iuscommunity.org/ius-release.rpm
echo '----- Install Python3.6 -----'
yum install -y python36u python36u-libs python36u-devel python36u-pip

echo '----- Install pip for Python3.6 -----'
python3.6 -m ensurepip
echo '----- Update pip -----'
python3.6 -m ensurepip --upgrade

echo '----- Install django module -----'
python3.6 -m pip install django
